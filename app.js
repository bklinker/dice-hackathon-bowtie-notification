angular.module('manageAlertsApp', [ 
    'ngResource', 
    'ngAnimate'
    ])
    
    //----PROVIDERS----
    //-----------------
    .provider('requestNotification', function(){
        // This is where we keep subscribed listeners
        var onRequestStartedListeners = [];
        var onRequestEndedListeners = [];
        // Utility to increment the request count
        var count = 0;
        var requestCounter = {
            increment: function () {
                count++;
            },
            decrement: function () {
                if (count > 0) count--;
            },
            getCount: function () {
                return count;
            }
        };
        // Subscribe to be notified when request starts
        this.subOnRequestStarted = function(listener){
            onRequestStartedListeners.push(listener);
        };
        // Tell provider that the request has started.
        this.fireRequestStarted = function(request){
            requestCounter.increment();
            //run each subscribed listener
            angular.forEach(onRequestStartedListeners, function(listener){
                // call the listener with request argument
                listener(request);
            });
            return request;
        };
        // this is a complete analogy to the Request START
        this.subOnRequestEnded = function(listener){
            onRequestEndedListeners.push(listener);
        };
        this.fireRequestEnded = function(){
            requestCounter.decrement();
            var passedArgs = arguments;
            angular.forEach(onRequestEndedListeners, function(listener){
                listener.apply(this, passedArgs);
            });
            return arguments[0];
        };
        this.getRequestCount = requestCounter.getCount;
        //returned as a service
        this.$get = function(){
            var that = this;
            // just pass all the 
            return {
                subOnRequestStarted: that.subOnRequestStarted,
                subOnRequestEnded: that.subOnRequestEnded,
                fireRequestEnded: that.fireRequestEnded,
                fireRequestStarted: that.fireRequestStarted,
                getRequestCount: that.getRequestCount
            };
        };
    })

    //----CONFIG----
    //--------------
    .config(['$httpProvider', 'requestNotificationProvider', function($httpProvider, requestNotificationProvider) {
        //initialize get if not there
        if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.get = {};    
        }
        //disable IE ajax request caching
        $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
        //rules for watching the requests for loading.
        $httpProvider.defaults.transformRequest.push(function(data){
            requestNotificationProvider.fireRequestStarted(data);
            return data;
        });
        $httpProvider.defaults.transformResponse.push(function(data){
            requestNotificationProvider.fireRequestEnded(data);
            return data;
        });
    }])

    //----DIRECTIVES----
    //------------------
    .directive('loadingWidget', ['requestNotification', '$animate', function(requestNotification, $animate){
        return{
            restrict: 'AE',
            contoller: function(scope){
                scope.listScope = false;
            },
            link: function(scope, element, attr, ctrl){
                scope.scopednotes = requestNotification;
                var loader = '<p><i class="icon-loading"></i> Loading Alerts</p>';
                var buttonMsg = element.html();
                scope.scopednotes.subOnRequestStarted(function(){
                    if(scope.listLoad === true){
                        $animate.addClass(element, 'loader-in');
                        element.html(loader);
                    }
                });
                scope.scopednotes.subOnRequestEnded(function(){
                    $animate.addClass(element, 'loader-out');
                    $animate.removeClass(element, 'loader-in');
                    element.html('');
                    scope.listLoad = false;
                });
            }
        }
    }])
    .directive('itemScope', function(){
        return{
            restrict: 'AE',
            controller: function($scope){
                $scope.itemLoading = false;
                this.setItemLoader = function(setLoader){
                    if(setLoader === true){
                        $scope.itemLoading = true;
                        $scope.$apply();
                    }
                }
            }
        }
    })
    .directive('deleteLoading', ['requestNotification', '$animate', function(requestNotification, $animate){
        return{
            require: '^itemScope',
            restrict: 'AE',
            link: function(scope, element, attr, ctrl){
                scope.deletenotes = requestNotification;
                var deleting = '<div class="item-notification"><p><i class="icon-loading"></i></p></div><div class="item-notification-backdrop"></div>';
                var updating = '<div class="itemNotification"></div>';
                scope.deletenotes.subOnRequestStarted(function(){
                    if(scope.itemLoading === true){
                        $animate.addClass(element, 'delete-in');
                        element.html(deleting);
                    }
                });
                scope.deletenotes.subOnRequestEnded(function(){
                    $animate.addClass(element, 'delete-out');
                    $animate.removeClass(element, 'delete-in');
                    element.html('');
                    scope.itemLoading = false;
                });
            }
        }
    }])
    .directive('toggleControl', function(){
        return{
            restrict: "AE",
            controller: function($scope){
                $scope.inputToggle = false;
                $scope.inputFocus = false;
                this.blurToggle = function(incoming){
                    if(incoming === true){
                        $scope.inputToggle = false;
                        $scope.$apply();
                    }
                };
            }
        }
    })
    .directive('staticToggle', ['$animate', function($animate){
        return{
            require: '^toggleControl',
            link: function(scope, element, attr, ctrl){
                scope.$watch('inputToggle', function(newValue, oldValue){
                    if(newValue === true){
                        element.addClass('ng-hide');
                    }else{
                        element.removeClass('ng-hide');
                    };
                });
            }
        }
    }])
    .directive('clickFocus', ['$animate', '$timeout', function($animate, $timeout){
        return{
            require: ['^toggleControl', '^itemScope'],
            link: function(scope, element, attr, ctrl){
                scope.$watch('inputToggle', function(newValue, oldValue){
                    if(newValue === true){
                        element.removeClass('ng-hide');
                    }else{
                        element.addClass('ng-hide');
                    }
                });
                scope.$watch('inputFocus', function(newValue, oldValue){
                    if(newValue === true){
                        element.focus();
                        scope.inputFocus = false;
                    };
                });
                element.bind('blur', function(){  
                    ctrl[0].blurToggle(scope.inputToggle);
                    var setLoader = true;
                    ctrl[1].setItemLoader(setLoader);
                });
            }
        };
    }])
    .directive('renameToggle', ['$animate', function($animate){
        return{
            require: '^toggleControl',
            link: function(scope, element, attr, crtl){
                scope.$watch('inputToggle', function(newValue, oldValue){
                    if(newValue === true){
                        element.attr('disabled', 'disabled');
                    }else if (newValue === false){
                        element.removeAttr('disabled');
                    }
                })
            }
        }
    }])
    .directive('setStatus', ['$rootScope', function($rootScope){
        return{
            link: function(scope, element, attr, ctrl){
                scope.$watch('$rootScope.activeStatus', function(){
                    console.log($rootScope.activeStatus);
                    $rootScope.$broadcast('$rootScope.activeStatus');
                    if($rootScope.activeStatus === 10 && scope.alert.status === '2'){
                        element.attr('disabled', 'disabled');
                    }
                    if($rootScope.activeStatus === 10 && scope.alert.status === null){
                        element.attr('disabled', 'disabled');
                    }
                })
                scope.$on('statusTotal', function(){
                    console.log($rootScope.activeStatus);
                    $rootScope.$broadcast('$rootScope.activeStatus');
                    if($rootScope.activeStatus === 10 && scope.alert.status === '2'){
                        element.attr('disabled', 'disabled');
                    }else if($rootScope.activeStatus <= 9 && scope.alert.status === '2'){
                        element.removeAttr('disabled');
                    }
                })
            }
        }
    }])
    //----SERVICES----
    //----------------
    .factory('Agents', ['$resource', 'apiDomain', function($resource, apiDomain){
        return {
            savedSearch: $resource('/proxy/index.php', {}, {
                query:{
                    method: "GET",
                    //params: {id: '@id', userId: '@userId'},
                    data: '',
                    headers: {
                        'Content-Type':'application/json; charset=utf-8'
                    }
                },
                save:{
                    method: "PUT",
                    params: {id: '@id', userId: '@userId'},
                    data: '',
                    headers: {
                        'Content-Type':'application/json; charset=utf-8'
                    }
                },
                remove:{
                    method: "DELETE",
                    params: {id: '@id', userId: '@userId'},
                    data: '',
                    headers: {
                        'Content-Type':'application/json; charset=utf-8'
                    }
                }
            }),
            postPush: $resource('push.php', {}, {
                push:{
                    method: "POST",
                    data: '',
                    header: {
                        'Content-Type':'application/json; charset=utf-8'
                    }
                }
            })
        }
    }])
    .factory('Sub', ['$q', function($q){
        return {
            getSub: function(){
                var deferred = $q.defer();
                var subId;
                setTimeout(function() {
                    navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {
                        // To unsubscribe from push messaging, you need get the
                        // subcription object, which you can call unsubscribe() on.
                        serviceWorkerRegistration.pushManager.getSubscription().then(function(pushSubscription) {
                            // Check we have a subscription to unsubscribe
                            if (!pushSubscription) {
                                // No subscription object, so set the state
                                // to allow the user to subscribe to push
                                isPushEnabled = false;
                                pushButton.disabled = false;
                                pushButton.textContent = 'Enable Push Messages';
                                return;
                            }
                            subId = pushSubscription.subscriptionId;
                            deferred.resolve(subId);
                        }).catch(function(e) {
                            window.Demo.debug.log('Error thrown while getting sub id.', e);
                            deferred.reject('NO ID');
                        });
                    }).catch(function(e) {
                        window.Demo.debug.log('Error thrown while unsubscribing from push messaging.', e);
                        deferred.reject('NO ID');
                    });
                }, 5000);
                return deferred.promise;
            }
        }
    }])
    //----CONTROLLERS----
    //-------------------
    .controller('manageCtrl', ['$rootScope', '$scope', 'Agents', 'Sub', 'userId', function($rootScope, $scope, Agents, Sub, userId){
        $rootScope.activeStatus = 0;
        $scope.alertFetch = function(){
            $scope.listLoad = true;
            Agents.savedSearch.query({userId: userId}).$promise.then(function(data){
                $rootScope.alerts = data.items;
                $scope.setAlertStatusTitle();
            }, function(e){
                $scope.noAgentAlerts = true;
            });
        }
        $scope.alertSingleFetch = function(alertId){
            //$scope.listLoad = true;
            Agents.savedSearch.query({userId: userId, id: alertId}).$promise.then(function(data){
                for(var i = 0; i < $rootScope.alerts.length; i++){
                    if(alertId === $rootScope.alerts[i].id){
                        $rootScope.alerts[i] = data;
                        $scope.setAlertStatusTitle();
                    }
                }
            });
        }
        $scope.alertUpdate = function(){
            if(this.alertForm.$valid){
                var alertId = this.alert.id;
                Agents.savedSearch.save({id: this.alert.id, userId: userId}, {ssearchName: this.alert.ssearchName, status: this.alert.status}).$promise.then(function(data){
                    $scope.alertSingleFetch(alertId);
                }, function(e){
                    console.log(e);
                });
            }
        };
        $scope.alertDestroy = function(alert){
            if($rootScope.alerts.length <= 1){
                Agents.savedSearch.remove({id: this.alert.id, userId: userId}, function(){
                    $rootScope.alerts.pop();
                    $scope.noAgentAlerts = true;
                });
            }else{
                var index = $scope.alerts.indexOf(alert);
                
                Agents.savedSearch.remove({id: this.alert.id, userId: userId}).$promise.then(function(data){
                    $rootScope.alerts.splice(index, 1);
                }, function(e){
                    console.log(e);
                });
            };
        };
        $scope.alertPush = function(){
            this.alert.status = this.value;
            var currSubId = Sub.getSub();
            currSubId.then(function(thisId){
                console.log(thisId);
                Agents.postPush.push({},{subscriptionID: thisId}).$promise.then(function(e){
                    console.log(e);
                });
            });
        };
        $scope.alertFrequency = function(){
            this.alert.status = this.value;
            $scope.setAlertStatusTitle();
            this.alertUpdate();
        };
        $scope.setAlertStatusTitle = function(){
            $rootScope.activeStatus = 0;
            for(var i = 0; i < $rootScope.alerts.length; i++){
                if($rootScope.alerts[i].status === '1'){
                    $rootScope.alerts[i].statusTitle = ' Daily';
                    $rootScope.activeStatus++;
                }else if($rootScope.alerts[i].status === '2' || $rootScope.alerts[i].status === null){
                    $rootScope.alerts[i].statusTitle = ' Never';
                }else{ }
            }
            $rootScope.$broadcast('statusTotal');
        };
        $scope.alertFetch();
    }]);