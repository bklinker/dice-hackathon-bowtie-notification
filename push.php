<?php
/**
 * Created by PhpStorm.
 * User: kim.spasaro
 * Date: 4/21/15
 * Time: 3:42 PM
 */

$ch = curl_init();

curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: key=AIzaSyCOWiBezBM1GaotPvjUu3RpNbWh0VCNsqM',
    'Content-Type: application/json'
));
curl_setopt($ch, CURLOPT_URL,"https://android.googleapis.com/gcm/send");
curl_setopt($ch, CURLOPT_POST, 1);
$registration_id = $_POST['subscriptionID'];
$data = array("registration_ids" => $registration_id);
curl_setopt($ch, CURLOPT_POSTFIELDS,$data);  //Post Fields
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);