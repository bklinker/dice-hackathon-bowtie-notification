<?php

// README
// http://guzzle.readthedocs.org/en/latest/overview.html

require 'vendor/autoload.php';

// Create Guzzle client
$client = new GuzzleHttp\Client();
$search_request = $client->createRequest('GET', 'http://api.diceqa.com/customers/1019612/savedSearches');
$search_query = $search_request->getQuery();
$search_response = $client->send($search_request)->getBody();
echo $search_response;

?>