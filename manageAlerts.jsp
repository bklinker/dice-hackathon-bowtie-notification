<!doctype html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en" xmlns:ng="http://angularjs.org" id="ng-app" ng-app="manageAlertsApp" ng-cloak >
<head>
    <c:import url="/content/includes/head-default.jsp"/>
    <title>Manage Search Agent Alerts</title>
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400italic,300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="${domainData.assetsDomain}/assets/stylesheets/ows/manage-alerts.css">
    <c:import url="/content/includes/respond-employer.jsp"/>
    <c:import url="/content/includes/scripts-default.jsp"/>
    <c:set var="userId" value="${sessionScope['USER_ID']}" scope="page"/>
</head>
<body>
    <c:import url="/servlet/CommonController?op=20004" context="/common"> </c:import>
    <div class="wrapper page-header-alerts">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Search Agent Alerts</h1> 
                </div>
            </div>
        </div>
    </div>
    <div class="container search-alerts" ng-controller="manageCtrl">
        <div class="row">
            <div class="col-md-12">
                <div class="row row-details">
                    <div class="col-xs-11 col-sm-8 col-md-8 col-lg-8">
                        <h3>Alert Title</h3>
                    </div>
                    <div class="col-xs-6 col-sm-2 col-md-2 col-md-offset-0 hidden-xs text-center">
                        <h3>Date Updated</h3>
                    </div>
                    <div class="col-xs-6 col-sm-2 col-md-2 col-md-offset-0 hidden-xs text-center">
                        <h3>Notification</h3>
                    </div>
                </div>
                <div class="loading-widget" loading-widget ></div>
                <div class="repeater-form" ng-if="noAgentAlerts">
                    <div class="row row-group">
                        <div class="col-sm-12 col-md-12">
                            <p>No existing saved searches were found. <a href="/ows/isearch.html">Start a new search</a> to save additional searches.</p>
                        </div>
                    </div>
                </div>
                <!-- BEGIN JOB ALERT NG-REPEAT -->
                <div class="base-ani repeater-form ng-cloak" ng-cloak ng-switch="warnings" ng-repeat="alert in alerts | orderBy:'lastModifiedDate':true" item-scope>
                    <!-- BEGIN INTERACTION MESSAGES -->
                    <div class="alert alert-warning base-ani" ng-switch-when="warningsDelete"><i class="icon-question-sign icon-white">
                        </i> Are you sure you want to delete this alert? <a id="delete-confirm-{{$index + 1}}" class="btn btn-link" ng-click="alertDestroy(alert); $parent.itemLoading = true; $parent.warnings='warningsNone'">Yes</a> <a id="delete-cancel-{{$index + 1}}" class="btn btn-link no-left-pad" ng-click="$parent.warnings='warningsNone'">Cancel</a>
                    </div>
                    <!-- END INTERACTION MESSAGES -->
                    <div class="row row-group">
                        <div delete-loading class="delete-loading">
                        </div>
                        <div id="alert-{{$index + 1}}" class="form-inline col-xs-12 col-sm-8 col-md-8 col-lg-8 main-col">
                            <div class="row" toggle-control >
                                <div class="form-group col-sm-12 no-left-pad"> 
                                    <div class="col-sm-12 col-md-12 alert-name-ani" >
                                        <form ng-submit="alertUpdate()" id="alert-form-{{$index + 1}}" class="form-inline" name="alertForm">
                                            <input id="alert-form-{{$index + 1}}" name="alertName" class="form-control name-input form-name" type="text" ng-blur="alertUpdate()" ng-model="alert.ssearchName" static-toggle click-focus ng-minlength=3 ng-maxlength=100 required />
                                            <h4 class="form-control name-input" static-toggle ><a id="alert-name-{{$index + 1}}" href="${domainData.employerDomain}/ows/isearch.html?lss={{alert.id}}">{{alert.ssearchName}}</a></h4>
                                        </form>
                                    </div>
                                </div>
                                <div class="form-group action-row col-sm-10">
                                    <div class="btn-group alert-btn-group">
                                        <a id="alert-rename-{{$index + 1}}" ng-click="inputToggle = true; inputFocus = true" class="btn btn-link rename-alert" type="button" rename-toggle >Rename</a>
                                        <a id="alert-delete-{{$index + 1}}" class="btn btn-link" ng-click="warnings='warningsDelete'">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div><!-- END FORM INLINE -->
                        <div class="form-group col-xs-6 col-sm-2 col-md-2 col-md-offset-0 text-center row-section">
                            <h3 class="hidden-sm hidden-md hidden-lg">Date Updated</h3>
                            <label id="lastmodified-date-{{$index + 1}}" class="alert-label">{{alert.formattedLastModifiedDate}}</label>
                        </div>
                        <div class="form-group col-xs-6 col-sm-2 col-md-2 text-center row-section">
                            <h3 class="hidden-sm hidden-md hidden-lg">Notifications</h3>
                            <button id="email-frequency-{{$index + 1}}" class="btn btn-link dropdown-toggle frequency-alert" data-toggle="dropdown" ng-model="alert.status" set-status ><i class="icon-envelope"></i>{{alert.statusTitle}} <span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu" tabindex="100">
                                <li><a id="alert-frequency-daily" href="#" ng-click="value='1';alertFrequency();">Daily</a></li>
                                <li><a id="alert-frequency-never" href="#" ng-click="value='2';alertPush();">Never</a></li>
                            </ul>
                        </div>
                    </div><!-- END ROW ROW GROUP -->
                </div><!-- END JOB ALERT NG-REPEAT -->
            </div>
        </div>
    </div>
    
    <c:import url="/provider/tbs-footer.jsp" context="/common" />
    <script src="${domainData.assetsDomain}/assets/lib/angular-1.2.16/angular.js"></script>
    <script src="${domainData.assetsDomain}/assets/lib/angular-1.2.16/resource/angular-resource.js"></script>
    <script src="${domainData.assetsDomain}/assets/lib/angular-1.2.16/animate/angular-animate.js"></script>
    <script src="${domainData.assetsDomain}/assets/scripts/ows/manage-alerts/app.js"></script>
    <script>
        (function() {
            var app = angular.module('manageAlertsApp');
            app.value('apiProxyUrl', '/daf/core/api/proxy.json');
            app.value('apiProxyUrlDice', '/daf/dice/core/api/proxy.json');
            app.value('apiDomain', '${domainData.apiDomain}');
            app.value('userId', '${userId}');
        })();
    </script>
</body>
</html>